import React from 'react';
import axios from 'axios';
import './App.css';

class App extends React.Component {
  constructor() {
    super()
    this.state = {
      backendStatus: '...'
    };
  }

  componentDidMount() {
    axios({method: 'get', url: 'http://localhost:796'})
    .then(() => this.setState({backendStatus: 'OK'}))
    .catch(() => this.setState({backendStatus: 'BAD'}));
  }

  render() {
    return (
      <div className="App">
        Hello from swk dev team, backend status: {this.state.backendStatus}
      </div>
    );
  }
}

export default App;
